module.exports = {
  entry: "./src/js/app.js",
  output: {
    filename: "bundle.js",
    path: __dirname + "/dist"
  },
  module: {
    rules: [
      { test: /\.css$/, use: "style-loader" },
      { test: /\.css$/, use: "css-loader" }
    ]
  }
};
